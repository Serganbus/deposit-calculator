<?php

namespace Serganbus\Money\Deposits;

use DateTime;
use DateInterval;

/**
 * Хранит и возвращает параметры депозита
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class DepositParams
{
    public const DURATION_DAY = 1;
    public const DURATION_MONTH = 2;
    public const DURATION_QUARTER = 3;
    public const DURATION_YEAR = 4;
    
    public static $daysInPaymentPeriod = [
        self::DURATION_DAY => 1,
        self::DURATION_MONTH => 30,
        self::DURATION_QUARTER => 91,
        self::DURATION_YEAR => 365,
    ];
    
    /**
     * @var DateTime Дата открытия вклада
     */
    protected $initialDate;
    
    /**
     * @var int Сумма вклада(в копейках)
     */
    protected $initialSum;

    /**
     * @var int|float Процентная ставка
     */
    protected $percents;
    
    /**
     * Выплата процентов в конце срока
     *
     * @var bool
     */
//    protected $percentsInTheEnd = true;
    
    /**
     * @var int|float|null Процентная ставка при досрочном закрытии
     */
    protected $earlyClosingPercents;
    
    /**
     * @var DateTime|null Дата досрочного закрытия
     */
    protected $earlyClosingDate;
    
    /**
     * @var int Тип продолжительности платежного периода: дни, недели, месяцы и т.д.
     */
    protected $durationType;
    
    /**
     * @var int Количество периодов, в течение которых действует депозит
     */
    protected $depositPeriodsCount;
    
    /**
     * @var bool Включена ли капитализация процентов
     */
    protected $capitalization = false;
    
    /**
     *
     * @param DateTime $initialDate
     * @param int $initialSum
     * @param int|float $percents
     * @param int $durationType
     * @param int $depositPeriodsCount
     * @throws \InvalidArgumentException
     */
    public function __construct(
        DateTime $initialDate,
        int $initialSum,
        $percents,
        int $durationType,
        int $depositPeriodsCount = 1
    ) {
        
        if (!in_array($durationType, array_keys(static::$daysInPaymentPeriod))) {
            throw new \InvalidArgumentException("Invalid duration type. Actual: {$durationType}");
        }
        if (!is_numeric($percents) || $percents < 0) {
            throw new \InvalidArgumentException("Invalid percents. Actual: {$percents}");
        }

        $this->initialDate = $initialDate;
        $this->initialSum = $initialSum;
        $this->percents = $percents;
        $this->durationType = $durationType;
        $this->depositPeriodsCount = $depositPeriodsCount;
    }
    
    /**
     * Добавление капитализации процентов по вкладу
     * @param bool $capitalization
     * @return $this
     */
    public function capitalization(bool $capitalization)
    {
        $this->capitalization = $capitalization;
        
        return $this;
    }
    
    /**
     * Является ли вклад с капитализацией процентов?
     *
     * @return bool
     */
    public function withCapitalization(): bool
    {
        return $this->capitalization;
    }
    
    /**
     * Установка параметра выплаты процентов.
     * Проценты можно выплачивать в конце срока или при начислении процентов
     *
     * @param bool $inTheEnd Выплата процентов в конце срока или нет
     * @return $this
     */
//    public function setPercentsPaymentInTheEnd(bool $inTheEnd)
//    {
//        $this->percentsInTheEnd = $inTheEnd;
//
//        return $this;
//    }
    
    /**
     * Получение параметра выплаты процентов.
     * Проценты можно выплачивать в конце срока или при начислении процентов
     *
     * @return bool
     */
//    public function isPercentsInTheEnd(): bool
//    {
//        return $this->percentsInTheEnd;
//    }
    
    /**
     * Добавление параметров по досрочному закрытию вклада
     * @param int|float $percents
     * @param DateTime $closingDate
     * @return $this
     */
    public function earlyClosing($percents, DateTime $closingDate)
    {
        if ($closingDate <= $this->initialDate) {
            throw new \InvalidArgumentException("Closing date should be greater than initial date of deposit");
        }
        if (!is_numeric($percents) || $percents < 0) {
            throw new \InvalidArgumentException("Invalid percents. Actual: {$percents}");
        }
        
        $this->earlyClosingPercents = $percents;
        $this->earlyClosingDate = $closingDate;
        
        return $this;
    }
    
    /**
     * Указывает, является ли депозит досрочно закрытым
     *
     * @return bool
     */
    public function isEarlyClosed(): bool
    {
        return !is_null($this->earlyClosingPercents)
                && !is_null($this->earlyClosingDate);
    }
    
    /**
     * Вернуть тип продолжительности платежного периода
     *
     * @return int
     */
    public function getDurationType(): int
    {
        return $this->durationType;
    }

    /**
     * Вернуть дату получения займа
     *
     * @return DateTime
     */
    public function getInitialDate(): DateTime
    {
        return $this->initialDate;
    }
    
    /**
     * Получить начальную сумму вклада
     *
     * @return int
     */
    public function getInitialSum(): int
    {
        return $this->initialSum;
    }
    
    /**
     * Вернуть проценты годовых по вкладу
     *
     * @return int|float
     */
    public function getPercents()
    {
        return $this->percents;
    }
    
    /**
     * Получить дату досрочного закрытия
     *
     * @return DateTime|null
     */
    public function getEarlyClosingDate()
    {
        return $this->earlyClosingDate;
    }
    
    /**
     * Получить процентную ставку при досрочном закрытии
     *
     * @return int|float|null
     */
    public function getEarlyClosingPercents()
    {
        return $this->earlyClosingPercents;
    }
    
    /**
     * Вернуть количество дней, сколько длится депозит
     *
     * @return int
     */
    public function getDepositDurationInDays(): int
    {
        $daysInDuration = self::$daysInPaymentPeriod[$this->durationType];
        
        return $daysInDuration * $this->depositPeriodsCount;
    }
    
    /**
     * Получить проценты в периоде депозита
     *
     * @return int|float
     */
    public function getPercentsInPeriod()
    {
        $daysInDuration = self::$daysInPaymentPeriod[$this->durationType];
        
        return $this->percents * $daysInDuration / 365;
    }
    
    /**
     * Вернуть даты выплат по депозиту
     *
     * @return array
     */
    public function getPaymentDates(): array
    {
        $dates = [];
        
        /** @var int $daysInDuration */
        $daysInDuration = self::$daysInPaymentPeriod[$this->durationType];
        
        $iterateInterval = new DateInterval("P{$daysInDuration}D");
        $initialDate = clone $this->initialDate;
        for ($i = 1; $i <= $this->depositPeriodsCount; $i++) {
            $initialDate->add($iterateInterval);
            $dates[] = clone $initialDate;
        }
        
        return $dates;
    }
}
