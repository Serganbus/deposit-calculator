<?php

namespace Serganbus\Money\Deposits;

use DateTime;
use DateInterval;

/**
 * Конфигурирование депозитного калькулятора
 * и расчет параметров депозита с его помощью
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class Calculator
{
    public function calculate(DepositParams $depositParams): array
    {
        /** @var DateTime $initialDate */
        $initialDate = $depositParams->getInitialDate();
        
        /** @var int $initialSum */
        $initialSum = $depositParams->getInitialSum();
        
        $result = [
            $initialDate->format('Y-m-d') => $initialSum
        ];
        
        // Если досрочное закрытие вклада
        if ($depositParams->isEarlyClosed()) {
            return $this->calculateWithEarlyClosingDate($depositParams, $result);
        }
        
        // если выплата процентов осуществляется через равные промежутки времени
        $paymentDates = array_map(function ($item) {
            return $item->format('Y-m-d');
        }, $depositParams->getPaymentDates());
        
        $total = $initialSum; // сумма, от которой рассчитываются проценты
        $gain = 0; // выплаты по процентам в моменте
        $percentsInPeriod = $depositParams->getPercentsInPeriod() / 100;
        
        $currentDate = clone $initialDate;
        $iterateInterval = new DateInterval('P1D');
        /** @var int $durationInDays */
        $durationInDays = $depositParams->getDepositDurationInDays();
        for ($i = 0; $i <= $durationInDays; $i++) {
            $currentDate->add($iterateInterval);
            
            $currentDateFormatted = $currentDate->format('Y-m-d');
            
            // является ли данный день днем окончания очередного срока по выплате процентов
            if (in_array($currentDateFormatted, $paymentDates)) {
                // начисляем проценты
                $gain += (int)round($total * $percentsInPeriod);
                
                if ($depositParams->withCapitalization()) {
                    // зачисляем выплаты процентов
                    $total += $gain;

                    // обнуляем выплаты по процентам
                    $gain = 0;
                    
                    $result[$currentDateFormatted] = $total;
                }
            }
        }
        
        $total += $gain;
        if (!$depositParams->withCapitalization()) {
            $result[$currentDate->format('Y-m-d')] = $total;
        }
        
        return $result;
    }
    
    protected function calculateWithEarlyClosingDate(DepositParams $depositParams, array $result)
    {
        /** @var DateTime $initialDate */
        $initialDate = $depositParams->getInitialDate();
        
        /** @var int $initialSum */
        $initialSum = $depositParams->getInitialSum();
        
        $earlyClosingDate = $depositParams->getEarlyClosingDate();
        $earlyClosingPercents = $depositParams->getEarlyClosingPercents() / 100;
        $diffInterval = $earlyClosingDate->diff($initialDate);
        $diffInDays = $diffInterval->days;
        $gain = (int)round($diffInDays * $earlyClosingPercents * $initialSum / 365);

        $result[$earlyClosingDate->format('Y-m-d')] = $initialSum + $gain;

        return $result;
    }
}
