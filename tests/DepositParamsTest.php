<?php

namespace Serganbus\Money\Deposits;

use PHPUnit\Framework\TestCase;

/**
 * Description of DepositParamsTest
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class DepositParamsTest extends TestCase
{
    /**
     * @var DepositParams
     */
    private $params;
    
    public function setUp(): void
    {
        $this->params = new DepositParams(
            new \DateTime('2019-10-31 00:00:00'), 
            10000000, 
            465, 
            DepositParams::DURATION_MONTH, 
            12
        );
    }
    
    public function testGetDurationType()
    {
        $this->assertEquals(DepositParams::DURATION_MONTH, $this->params->getDurationType());
    }
    
    public function testGetInitialDate()
    {
        $date = new \DateTime('2019-10-31 00:00:00');
        $this->assertEquals($date->format('Y-m-d'), $this->params->getInitialDate()->format('Y-m-d'));
    }
    
    public function testGetInitialSum()
    {
        $this->assertEquals(10000000, $this->params->getInitialSum());
    }
    
    public function testGetPercents()
    {
        $this->assertEquals(465, $this->params->getPercents());
    }
    
    public function testEarlyClosing()
    {
        $this->assertInstanceOf(DepositParams::class, $this->params->earlyClosing(1, new \DateTime('2020-01-01')));
        
        $this->expectException(\InvalidArgumentException::class);
        $this->params->earlyClosing(1, new \DateTime('2019-01-01'));
    }
    
    public function testIsEarlyClosed()
    {
        $this->assertFalse($this->params->isEarlyClosed());
        
        $this->params->earlyClosing(1, new \DateTime('2020-01-01'));
        $this->assertTrue($this->params->isEarlyClosed());
    }
    
    public function testGetEarlyClosingDate()
    {
        $this->assertNull($this->params->getEarlyClosingDate());
        $closingDate = new \DateTime;
        $this->params->earlyClosing(1, $closingDate);
        $this->assertEquals($closingDate->format('Y-m-d'), $this->params->getEarlyClosingDate()->format('Y-m-d'));
    }
    
    public function testGetEarlyClosingPercents()
    {
        $this->assertNull($this->params->getEarlyClosingPercents());
        $this->params->earlyClosing(1, new \DateTime);
        $this->assertEquals(1, $this->params->getEarlyClosingPercents());
    }
    
    public function testCapitalization()
    {
        $this->assertInstanceOf(DepositParams::class, $this->params->capitalization(true));
    }
    
    public function testWithCapitalization()
    {
        $this->assertFalse($this->params->withCapitalization());
        $this->params->capitalization(true);
        $this->assertTrue($this->params->withCapitalization());
    }
    
    public function testGetDepositDurationInDays()
    {
        $this->assertEquals(360, $this->params->getDepositDurationInDays());
    }
    
    public function testGetPercentsInPeriod()
    {
        $this->assertEqualsWithDelta(38.22, $this->params->getPercentsInPeriod(), 0.01);
    }
    
    public function testGetPaymentDates()
    {
        $dates = [
            '30.11.2019', '30.12.2019', '29.01.2020', '28.02.2020',
            '29.03.2020', '28.04.2020', '28.05.2020', '27.06.2020',
            '27.07.2020', '26.08.2020', '25.09.2020', '25.10.2020',
        ];
        $paymentDates = $this->params->getPaymentDates();
        $this->assertTrue(is_array($paymentDates));
        $this->assertEquals(12, count($paymentDates));
        $i = 0;
        foreach ($paymentDates as $paymentDate) {
            $this->assertInstanceOf(\DateTime::class, $paymentDate);
            $this->assertEquals($dates[$i], $paymentDate->format('d.m.Y'));
            $i++;
        }
    }
    
    public function tearDown(): void
    {
        $this->params = null;
    }
}
