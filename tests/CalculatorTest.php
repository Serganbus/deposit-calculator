<?php

namespace Serganbus\Money\Deposits;

use PHPUnit\Framework\TestCase;

/**
 * Description of CalculatorTest
 *
 * @author Sergey Ivanov <sivanovkz@gmail.com>
 */
class CalculatorTest extends TestCase
{
    /**
     * @var Calculator
     */
    private $calculator;
    
    public function setUp(): void
    {
        $this->calculator = new Calculator;
    }
    
    public function calculateDataProvider()
    {
        $from = new \DateTime('2020-03-01');
        $params1 = new DepositParams($from, 12000000, 7.5, DepositParams::DURATION_DAY, 180);
        $params2 = new DepositParams($from, 12000000, 7.5, DepositParams::DURATION_MONTH, 6);
        $params3 = new DepositParams($from, 12000000, 7.5, DepositParams::DURATION_YEAR, 1);
        $paramsWithEarlyClosing = new DepositParams($from, 12000000, 7.5, DepositParams::DURATION_YEAR, 1);
        $paramsWithEarlyClosing->earlyClosing(0.01, new \DateTime('2020-04-01'));
        
        $paramsWithCapitalization1 = new DepositParams($from, 12000000, 7.7, DepositParams::DURATION_DAY, 30);
        $paramsWithCapitalization1->capitalization(true);
        $paramsWithCapitalization2 = new DepositParams($from, 12000000, 6.6, DepositParams::DURATION_MONTH, 6);
        $paramsWithCapitalization2->capitalization(true);
        $paramsWithCapitalization3 = new DepositParams($from, 12000000, 5.5, DepositParams::DURATION_QUARTER, 8);
        $paramsWithCapitalization3->capitalization(true);
        return [
            [
                $params1,
                [
                    '2020-03-01' => 12000000,
                    '2020-08-29' => 12443880,
                ]
            ],
            [
                $params2,
                [
                    '2020-03-01' => 12000000,
                    '2020-08-29' => 12443838,
                ]
            ],
            [
                $params3,
                [
                    '2020-03-01' => 12000000,
                    '2021-03-02' => 12900000,
                ]
            ],
            [
                $paramsWithEarlyClosing,
                [
                    '2020-03-01' => 12000000,
                    '2020-04-01' => 12000102,
                ]
            ],
            [
                $paramsWithCapitalization1,
                [
                    '2020-03-01' => 12000000,
                    '2020-03-02' => 12002532,
                    '2020-03-03' => 12005064,
                    '2020-03-04' => 12007597,
                    '2020-03-05' => 12010130,
                    '2020-03-06' => 12012664,
                    '2020-03-07' => 12015198,
                    '2020-03-08' => 12017733,
                    '2020-03-09' => 12020268,
                    '2020-03-10' => 12022804,
                    '2020-03-11' => 12025340,
                    '2020-03-12' => 12027877,
                    '2020-03-13' => 12030414,
                    '2020-03-14' => 12032952,
                    '2020-03-15' => 12035490,
                    '2020-03-16' => 12038029,
                    '2020-03-17' => 12040569,
                    '2020-03-18' => 12043109,
                    '2020-03-19' => 12045650,
                    '2020-03-20' => 12048191,
                    '2020-03-21' => 12050733,
                    '2020-03-22' => 12053275,
                    '2020-03-23' => 12055818,
                    '2020-03-24' => 12058361,
                    '2020-03-25' => 12060905,
                    '2020-03-26' => 12063449,
                    '2020-03-27' => 12065994,
                    '2020-03-28' => 12068539,
                    '2020-03-29' => 12071085,
                    '2020-03-30' => 12073632,
                    '2020-03-31' => 12076179,
                ]
            ],
            [
                $paramsWithCapitalization2,
                [
                    '2020-03-01' => 12000000,
                    '2020-03-31' => 12065096,
                    '2020-04-30' => 12130545,
                    '2020-05-30' => 12196349,
                    '2020-06-29' => 12262510,
                    '2020-07-29' => 12329030,
                    '2020-08-28' => 12395911,
                ]
            ],
            [
                $paramsWithCapitalization3,
                [
                    '2020-03-01' => 12000000,
                    '2020-05-31' => 12164548,
                    '2020-08-30' => 12331352,
                    '2020-11-29' => 12500444,
                    '2021-02-28' => 12671854,
                    '2021-05-30' => 12845615,
                    '2021-08-29' => 13021758,
                    '2021-11-28' => 13200317,
                    '2022-02-27' => 13381324,
                ]
            ],
        ];
    }
    
    /**
     * @dataProvider calculateDataProvider
     */
    public function testCalculate(DepositParams $params, $expectedResult)
    {
        $result = $this->calculator->calculate($params);
        $this->assertTrue(is_array($result));
        
        $expectedDays = array_keys($expectedResult);
        $i = 0;
        foreach ($result as $actualDate => $actualValue) {
            $this->assertEquals($expectedDays[$i], $actualDate);
            $this->assertEquals($expectedResult[$expectedDays[$i]], $actualValue, "Неверная сумма на дату {$actualDate}");
            
            $i++;
        }
    }
    
    public function tearDown(): void
    {
        $this->calculator = null;
    }
}
